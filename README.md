# M.Y.K.O.T.A

Minetest mod that adds mykota and dry mykota blocks.


M.Y.K.O.T.A is mushroom-like liveform. It grows in horizontal direction and falls down.
If mykota cant grow (adjacent blocks are occupied) then it turns to dry mykota and stop grow.

Dry mykota can be used as fuel.

## Settings

**mykota_fertility** - number of iterations after which it dies. Default is 5

Example:

```mykota_fertility = 13```
